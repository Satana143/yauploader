#-------------------------------------------------
#
# Project created by QtCreator 2011-01-12T15:43:01
#
#-------------------------------------------------

QT       += core gui
QT +=network

TARGET = yandex
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    classes/yandex.cpp \
    classes/auth.cpp \
    classes/albumstruct.cpp \
    classes/album.cpp \
    classes/upload.cpp


HEADERS  += mainwindow.h \
    classes/yandex.h \
    classes/auth.h \
    classes/albumstruct.h \
    classes/album.h \
    classes/upload.h

FORMS    += mainwindow.ui

RESOURCES += resource.qrc
