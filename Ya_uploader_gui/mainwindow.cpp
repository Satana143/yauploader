#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "classes/albumstruct.h"

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    this->setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMinimizeButtonHint);
    readOptions();

    createActions();
    createTrayIcon();
    connect(trayIcon,SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this,SLOT(showHide(QSystemTrayIcon::ActivationReason)));

    //интерфейс
    connect(ui->actionConnect,SIGNAL(triggered()),this,SLOT(showHideConnect()));
    connect(ui->actionUpload,SIGNAL(triggered()),this,SLOT(showHideUpload()));
    connect(ui->actionQuit,SIGNAL(triggered()),this,SLOT(close()));
    connect(ui->saveLoginPass,SIGNAL(stateChanged(int)),this,SLOT(saveOptions()));
    //первоначальные настройки интерфейса
   ui->btnAddFoto->setEnabled(false);
   ui->btnUpload->setEnabled(false);
   ui->btnDellFoto->setEnabled(false);
   ui->cbAlbumList->setEnabled(false);

    ui->widgetConnect->show();
    connectVisible=true;
    uploadVisible=false;

    ui->widgetUpload->hide();
    ui->console->hide();

   MainWindow::adjustSize();
}

MainWindow::~MainWindow()
{
    trayIcon->hide();
    delete ui;
}
//делаем иконку в трее
void MainWindow::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":/icons/icons/tray.png"));
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->show();
}
//сворачивание в трей
void MainWindow::showHide(QSystemTrayIcon::ActivationReason r)
{
    switch (r) {
    case QSystemTrayIcon::Trigger:
    case QSystemTrayIcon::DoubleClick:
       {
        if(!this->isVisible()){
            this->show();
        }else
            this->hide();

        break;
       }
    case QSystemTrayIcon::MiddleClick:
    default:
        ;
    }
}
//контекстное меню в трее
void MainWindow::createActions()
{
    uploadAction = new QAction(tr("Upload"),this);
    connect(uploadAction,SIGNAL(triggered()),this,SLOT(on_btnUpload_clicked()));
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}
//сообщение в трее
void MainWindow::showMessage()
{
    trayIcon->showMessage("Finish", "Upload is Compleate!", QSystemTrayIcon::Information, 10000);
}
//запуск авторизации
void MainWindow::on_btnConnect_clicked()
{
    if(ui->tbLogin->text().isEmpty() || ui->tbPass->text().isEmpty())
        QMessageBox::information(0,"Error!","Login or Password are Empty!",0);
    else
    {
    ui->btnConnect->setEnabled(false);
    ui->tbLogin->setEnabled(false);
    ui->tbPass->setEnabled(false);

    login = ui->tbLogin->text();
    pass = ui->tbPass->text();

    auth = new Auth();
    auth->log=login;
    auth->pass=pass;
    auth->goLogin();
    connect(auth,SIGNAL(finish(bool)),this,SLOT(changeAuth(bool)));
    connect(auth,SIGNAL(errorAuth()),this,SLOT(errorAuth()));
    }
}
void MainWindow::changeAuth(bool stat)
{
    if (stat)
    {
        //после авторизации врубаем запрос альбомов
        album = new Album();
        album->getAlbum(login);
        connect(album,SIGNAL(finish()),this,SLOT(fillAlbums()));
    }else
    {
        ui->btnAddFoto->setEnabled(false);
        ui->btnUpload->setEnabled(false);
    }
}
void MainWindow::fillAlbums()
{
    for(int i=0;i<album->getCountAlbum();i++)
    {
        ui->cbAlbumList->addItem(album->getTitleAlbum(i));
    }
    //блокируем логин,пас и выводим сообщение о конце авторизации
    ui->btnAddFoto->setEnabled(true);   
    ui->btnDellFoto->setEnabled(true);
    ui->cbAlbumList->setEnabled(true);
    ui->actionUpload->trigger();
}

//добавление списка загружаемых фотографий
void MainWindow::on_btnAddFoto_clicked()
{
    upload = new Upload();
    upload->add_foto(this);
    ui->btnUpload->setEnabled(true);
    refreshListFoto();
}
void MainWindow::on_btnUpload_clicked()
{
    upload->uploadF(auth->Authorization,album->getLincPhoto(ui->cbAlbumList->currentIndex()));

    connect(upload,SIGNAL(finish()),this,SLOT(refreshListFoto()));
    connect(upload, SIGNAL(uploadProgress(qint64,qint64)), this, SLOT(dataTransferProgress(qint64 , qint64)));
    connect(upload,SIGNAL(finishList()),this,SLOT(showMessage()));
}

void MainWindow::on_btnDellFoto_clicked()
{
    upload->removeFoto(ui->listView->selectionModel()->currentIndex().row());
    refreshListFoto();
}
void MainWindow::refreshListFoto()
{
    QAbstractItemModel *model=new QStringListModel(upload->files);
    ui->listView->setModel(model);
}

//прогресс загрузки фото
void MainWindow::dataTransferProgress(qint64 done, qint64 total)
{
    ui->progressBar->setMaximum(total);
    ui->progressBar->setValue(done);
}

void MainWindow::showHideConnect()
{
    if(!connectVisible){
        connectVisible=true;
        uploadVisible=false;
        ui->widgetUpload->hide();
        ui->widgetConnect->show();        
    }else
    {
        connectVisible=false;
    }
     MainWindow::adjustSize();
}
void MainWindow::showHideUpload()
{
    if(!uploadVisible){
        uploadVisible=true;
        connectVisible=false;
        ui->widgetConnect->hide();
        ui->widgetUpload->show();
    }else
    {
        uploadVisible=false;
    }
    MainWindow::adjustSize();
}

void MainWindow::saveOptions()
{
    QSettings *settings = new QSettings("settings.ini",QSettings::IniFormat);
    if(ui->saveLoginPass->checkState()==Qt::Checked){
    settings->setValue("Login",login);
    settings->setValue("Password",pass);
    settings->setValue("SaveLoginPass",true);
    }else{
        settings->setValue("Login","");
        settings->setValue("Password","");
        settings->setValue("SaveLoginPass",false);
    }
    settings->sync();
}

void MainWindow::readOptions()
{
    QSettings *settings = new QSettings("settings.ini",QSettings::IniFormat);
    login=settings->value("Login").toString();
    pass=settings->value("Password").toString();
    ui->saveLoginPass->setChecked(settings->value("SaveLoginPass").toBool());
    ui->tbLogin->setText(login);
    ui->tbPass->setText(pass);
}

void MainWindow::errorAuth()
{
    ui->btnConnect->setEnabled(true);
    ui->tbLogin->setEnabled(true);
    ui->tbPass->setEnabled(true);
}
