#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "classes/auth.h"
#include "classes/album.h"
#include "classes/upload.h"

#include <QMainWindow>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>

#include <QStringList>
#include <QString>

#include <QFileDialog>
#include <QProcess>
#include <QMessageBox>

#include <QAbstractItemModel>
#include <QStringListModel>
#include <QSystemTrayIcon>
#include <QMenu>
#include <QAction>
#include <QSettings>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    QSystemTrayIcon *trayIcon;
    //работа с сетью
    QNetworkAccessManager manager;
    QNetworkReply *reply;
    QUrl apiUrl;
    QString answer;

    //список загружаемых фоток
    QStringList files;
private:
    Ui::MainWindow *ui;
    //трей
    void createTrayIcon();
    void createActions();
    QAction *uploadAction;
    QAction *minimizeAction;
    QAction *restoreAction;
    QAction *quitAction;
    QMenu *trayIconMenu;

    //логин и пароль
    QString login;
    QString pass;

    //авторизация
        Auth *auth;
    //альбомы
        Album *album;
    //upload
        Upload *upload;
    //connectWidget visible
        bool connectVisible;
    //uploadWidget visible
        bool uploadVisible;
private slots:

        void on_btnConnect_clicked();
        void on_btnAddFoto_clicked();
        void on_btnUpload_clicked();
        void on_btnDellFoto_clicked();

    void changeAuth(bool stat);     //стадия авторизации
    void fillAlbums();              //заполняем бокс с альбомами
    void refreshListFoto();
    void dataTransferProgress(qint64 done, qint64 total);
//трей
    void showHide(QSystemTrayIcon::ActivationReason r);
    void showMessage();
//интерфейс
    void showHideConnect();
    void showHideUpload();
    void saveOptions();
    void readOptions();
    void errorAuth();
};

#endif // MAINWINDOW_H
