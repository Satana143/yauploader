#ifndef YANDEX_H
#define YANDEX_H

#include <QRegExp>
#include <QString>
#include <QStringList>
#include <QProcess>
#include <QFile>
#include <QByteArray>
#include <QMessageBox>

class Yandex
{
public:
    Yandex();
    //переменные
    QString key, id, token;
    QString encryptString;

   //методы
   bool Encrypt(QString login, QString pass);
   QString ParsStr(QString data,QString begin,QString end);
   QStringList ParsList(QString data,QString begin,QString end);
   void ParsKeyId(QString str);
   void ParsTocken(QString str);
};

#endif // YANDEX_H
