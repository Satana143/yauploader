﻿#ifndef ALBUM_H
#define ALBUM_H

#include "albumstruct.h"

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <QRegExp>
#include <QString>
#include <QStringList>
#include <QList>
#include <QTimer>
#include <QXmlStreamReader>
#include <QMessageBox>

class Album : public QObject
{
    Q_OBJECT
public:
    explicit Album(QObject *parent = 0);

    QNetworkAccessManager manager;
    QNetworkReply *reply;
    QUrl apiUrl;
    QString answer;

    //контейнер со структурой
    QList<AlbumStruct *> list;
    //запрос списка альбомов
    void getAlbum(QString login);
    //получение title альбома
    QString getTitleAlbum(int index);

    //получить ссылку на альбом
    QString getLincPhoto(int index);
    QString getLincEdit(int index);
    QString getLincSelf(int index);
    int getCountAlbum();

signals:
    void finish();//конец обработки

private slots:
    void readyReadReply();
    void getReplyFinished();
    //парс
    void parsAlbumInfo();
private:
   //парс записи
    void parsAlbums();
    QList<AlbumStruct *> parsEntry(QXmlStreamReader &xml);
};
#endif // ALBUM_H
