#ifndef UPLOAD_H
#define UPLOAD_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <QString>
#include <QStringList>
#include <QByteArray>
#include <QFileDialog>
#include <QMessageBox>
#include <QList>

class Upload : public QObject
{
    Q_OBJECT
public:
    explicit Upload(QObject *parent = 0);
    QNetworkAccessManager manager;
    QNetworkReply *reply;
    QUrl apiUrl;
    QString answer;

    QStringList files; //список загр фоток

    void add_foto(QWidget *parent);
    void uploadF(QByteArray tocken,QString url);
    void removeFoto(int index);
private:
    QByteArray tocken;
    QString url;
signals:
    void finish();
    void uploadProgress(qint64 , qint64);
    void finishList();
private slots:
    void readyReadReply();
    void getReplyFinished();
    void uploadS();
    void progress(qint64 x, qint64 x1);
};

#endif // UPLOAD_H
