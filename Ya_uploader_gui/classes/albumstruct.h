#ifndef ALBUMSTRUCT_H
#define ALBUMSTRUCT_H

#include <QString>

class AlbumStruct
{
public:
    AlbumStruct();
    AlbumStruct(QString Title,QString Linc,QString Count);

    QString title;
    QString selfLink;
    QString editLink;
    QString photosLink;

};

#endif // ALBUMSTRUCT_H
