#include "album.h"

Album::Album(QObject *parent) :
    QObject(parent)
{
}

void Album::getAlbum(QString login)
{
    apiUrl="http://api-fotki.yandex.ru/api/users/"+login+"/albums/";
    QNetworkRequest request(apiUrl);
    reply = manager.get(request);

    connect(reply, SIGNAL(finished()),this, SLOT(getReplyFinished()));
    connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadReply()));

    connect(reply,SIGNAL(finished()),this,SLOT(parsAlbumInfo()));
}

void Album::readyReadReply()
{
    answer.append(QString::fromUtf8(reply->readAll()));
}
void Album::getReplyFinished()
{
    reply->deleteLater();
}

void Album::parsAlbumInfo()
{
    parsAlbums();
}
//////////////////парс xml
//парс записи
void Album::parsAlbums()
{
    QXmlStreamReader xml(answer);
    QList<AlbumStruct *> listEntry;

    while (!xml.atEnd() && !xml.hasError()){
       QXmlStreamReader::TokenType token = xml.readNext();
       if(token==QXmlStreamReader::StartElement){
               if(xml.name()=="entry"){
                  listEntry.append(this->parsEntry(xml));
               }
       }
    }
    if(xml.hasError())
    {
        QMessageBox::critical(0,"Error",xml.errorString());
    }

     xml.clear();
     list= listEntry;
     //сигнал о конце обработки альбомов
     emit(finish());
}
//парс альбома
QList<AlbumStruct *> Album::parsEntry(QXmlStreamReader &xml)
{
    QList<AlbumStruct *> list;
    AlbumStruct *album = new AlbumStruct();

    xml.readNext();
    while(!(xml.tokenType()==QXmlStreamReader::EndElement &&
            xml.name()=="entry")){

        if(xml.tokenType() == QXmlStreamReader::StartElement) {
            if(xml.name()=="title"){

                if(xml.readNext()==QXmlStreamReader::Characters){
                  album->title=xml.text().toString();
                }

            }
          //парсим ссылки на альбомы
            if(xml.isStartElement() && xml.name()=="link"){
                QXmlStreamAttributes attrs = xml.attributes();
                QStringRef fHref = attrs.value("href");
                QStringRef fRel = attrs.value("rel");

                if(fHref.isEmpty() || fRel.isEmpty())
                {
                    xml.readNext();
                    continue;
                }

                if(fRel=="self"){
                album->selfLink=fHref.toString();
                }

                if(fRel=="edit"){
                album->editLink=fHref.toString();
                }

                if(fRel=="photos"){
                album->photosLink=fHref.toString();
                }
            }
        }
        xml.readNext();
    }
    list.append(album);
    return list;
}

//получение данных
QString Album::getTitleAlbum(int index)
{
    AlbumStruct *elem = list.at(index);
    return elem->title;
}
QString Album::getLincPhoto(int index)
{
    AlbumStruct *elem = list.at(index);
    return elem->photosLink;
}
QString Album::getLincEdit(int index)
{
    AlbumStruct *elem = list.at(index);
    return elem->editLink;
}
QString Album::getLincSelf(int index)
{
    AlbumStruct *elem = list.at(index);
    return elem->selfLink;
}

int Album::getCountAlbum()
{
    return list.length();
}
