#include "upload.h"

Upload::Upload(QObject *parent) :
    QObject(parent)
{
}

void Upload::add_foto(QWidget *parent)
{
    files = QFileDialog::getOpenFileNames(parent, tr("Select Music Files"),
                                                      tr("C:\\"));
}
void Upload::uploadF(QByteArray tocken, QString url)
{
    this->tocken=tocken;
    this->url = url;
    uploadS();
}

void Upload::removeFoto(int index)
{
    files.removeAt(index);
}

void Upload::uploadS()
{
    if (files.isEmpty())
        QMessageBox::information(0,"Error!","Not Foto in process!",0);
    else
        {
        int index=files.length()-1;
            if(index>=0)
            {
                QFile image(files[index]);

                QByteArray fileName,photos;

                QByteArray data;

                image.open(QIODevice::ReadOnly);
                data=image.readAll();
                image.close();

                apiUrl=url;
                photos.append(url);

                QNetworkRequest request(apiUrl);

                fileName.append(image.fileName());

                request.setRawHeader("POST", photos);
                request.setRawHeader("Host", "api-fotki.yandex.ru");
                request.setRawHeader("Content-Type", "image/jpeg");
                request.setRawHeader("Content-Length",QString::number(data.length()).toUtf8() );
                request.setRawHeader("Slug",fileName);
                request.setRawHeader("Authorization", tocken);

                reply = manager.post(request,data);

                connect(reply, SIGNAL(finished()),this, SLOT(getReplyFinished()));
                connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadReply()));
                connect(reply, SIGNAL(uploadProgress(qint64 , qint64)), this, SLOT(progress(qint64 , qint64)));
            }
    }
}
//удаление запроса
void Upload::getReplyFinished()
{
reply->deleteLater();
}
//ответ сервера
void Upload::readyReadReply()
{
        answer = QString::fromUtf8(reply->readAll());


        if(!files.isEmpty())
        {
            files.removeLast();
        }
        else return;


        if(!files.isEmpty())
        {
            emit finish();
            connect(reply, SIGNAL(finished()),this, SLOT(uploadS()));
        } else
        {
            files.clear();
            tocken.clear();
            url.clear();
            reply->deleteLater();
            emit finish();
            emit finishList();
            return;
        }

}

void Upload::progress(qint64 x, qint64 x1)
{
    emit uploadProgress(x,x1);
}
