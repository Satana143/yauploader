#ifndef AUTH_H
#define AUTH_H

#include "yandex.h"

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <QString>
#include <QByteArray>

class Auth : public QObject
{
    Q_OBJECT        
public:
    explicit Auth(QObject *parent = 0);

    void KeyId();
    QByteArray Authorization; //автаризационный токен

    QString log;
    QString pass;

private:
    QNetworkAccessManager manager;
    QNetworkReply *reply;
    QUrl apiUrl;
    QString answer;

    Yandex ya;//парс и шифрование
    int stage;//стадия авторизации
    int authCount;
signals:
    void finish(bool);
    void errorAuth();
public slots:
    void readyReadReply();
    void getReplyFinished();
    void login();
    void goLogin();
};

#endif // AUTH_H
