#-------------------------------------------------
#
# Project created by QtCreator 2012-12-06T20:00:26
#
#-------------------------------------------------

QT       += core

QT       -= gui
QT +=network
TARGET = Ya_uploader_consol
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    classes/auth.cpp \
    classes/yandex.cpp \
    classes/mainclass.cpp \
    classes/upload.cpp

HEADERS += \
    classes/auth.h \
    classes/yandex.h \
    classes/upload.h \
    classes/mainclass.h
