#include <QtCore/QCoreApplication>
#include <iostream>
#include <QFile>
#include <QFileInfo>
#include"classes/mainclass.h"

using namespace std;

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    if(argc<3) cout<<"Errors parametr"<<endl;
    else{
        if(QFile::exists("encrypt.exe")){
            MainClass *upl = new MainClass(argv[1],argv[2],argv[3]);
            QObject::connect(upl, SIGNAL(quit()), qApp, SLOT(quit()));
            upl->start();
         }else
             cout<<"Encryption software is not found!"<<endl;
    }
    return a.exec();
}
