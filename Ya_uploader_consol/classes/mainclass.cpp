#include "mainclass.h"

#define MAX_RETRY 5

using namespace std;
MainClass::MainClass(QObject *parent) :
    QObject(parent)
{
}
MainClass::MainClass(QString login, QString pass, QString image)
{
    this->image=image;
    this->login=login;
    this->pass=pass;
    retry_auth_count=0;
}
void MainClass::start(){
    cout<<"Login..."<<endl;
    auth=new Auth(login,pass);
    auth->goLogin();
    connect(auth,SIGNAL(finish()),this,SLOT(upload()));
    connect(auth,SIGNAL(errorAuth()),this,SLOT(errorAuth()));
}

void MainClass::errorAuth(){
     if(retry_auth_count<MAX_RETRY)
         auth->goLogin();
     else
         cout<<"Authorization failed"<<endl;
 }

 void MainClass::upload()
 {
    // cout<<"Upload..."<<endl;
     QByteArray tocken =auth->getTocken();

     QString url="http://api-fotki.yandex.ru/api/users/"+this->login+"/photos/";
     Upload *upload = new Upload(tocken,url,this->image);
     upload->start();
     connect(upload,SIGNAL(finish()),this,SLOT(finish()));
 }

 void MainClass::finish()
 {
     cout<<"Upload is complete"<<endl;
     emit quit();
 }
