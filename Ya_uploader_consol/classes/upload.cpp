#include "upload.h"

Upload::Upload(QObject *parent) :
    QObject(parent)
{
}
Upload::Upload(QByteArray tocken, QString url, QString image){
    this->tocken=tocken;
    this->url=url;
    this->image_path=image;
}
void Upload::start()
{
    QFile image(image_path);

    QByteArray fileName,photos;

    QByteArray data;

    image.open(QIODevice::ReadOnly);
    data=image.readAll();
    image.close();

    QUrl apiUrl=url;
    photos.append(url);

    QNetworkRequest request(apiUrl);

    fileName.append(image.fileName());

    request.setRawHeader("POST", photos);
    request.setRawHeader("Host", "api-fotki.yandex.ru");
    request.setRawHeader("Content-Type", "image/jpeg");
    request.setRawHeader("Content-Length",QString::number(data.length()).toUtf8() );
    request.setRawHeader("Slug",fileName);
    request.setRawHeader("Authorization", tocken);

    reply = manager.post(request,data);

    connect(reply, SIGNAL(finished()),this, SLOT(getReplyFinished()));
    connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadReply()));
}
void Upload::getReplyFinished()
{
reply->deleteLater();
}
void Upload::readyReadReply()
{
        QString answer = QString::fromUtf8(reply->readAll());
        emit finish();
}
