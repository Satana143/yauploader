#ifndef UPLOAD_H
#define UPLOAD_H

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <QByteArray>
#include <QFile>
#include <QString>

class Upload : public QObject
{
    Q_OBJECT
public:
    explicit Upload(QObject *parent = 0);    
    Upload(QByteArray tocken, QString url, QString image);
    void start();
private:
    QByteArray tocken;
    QString url;
    QString image_path;

    QNetworkAccessManager manager;
    QNetworkReply *reply;
    QUrl apiUrl;
    QString answer;
signals:
    void finish();
private slots:
    void getReplyFinished();
    void readyReadReply();
};

#endif // UPLOAD_H
