#ifndef MAINCLASS_H
#define MAINCLASS_H

#include <QObject>
#include <iostream>
#include "auth.h"
#include "yandex.h"
#include "upload.h"

class MainClass : public QObject
{
    Q_OBJECT
public:
    explicit MainClass(QObject *parent = 0);    
    MainClass(QString login, QString pass,QString image);
    void start();
private:
    Auth *auth;
    int retry_auth_count;
    QString image;
    QString login;
    QString pass;
signals:
    void quit();
private slots:
    void errorAuth();
    void upload();
    void finish();
};

#endif // MAINCLASS_H
