#include "yandex.h"
#include <iostream>

using namespace std;
Yandex::Yandex()
{
}
bool Yandex::Encrypt(QString RsaKey, QString credentials)
{
        QProcess proc;
        QStringList argum;
        QByteArray b;

        argum.clear();
        b.clear();

        argum << RsaKey;
        argum << credentials;

        if(QFile::exists("encrypt.exe"))
        {
        proc.start("encrypt.exe",argum);
        }else
        {
         cout<<"Error: Encryption software is not found!"<<endl;
         return false;
        }

        if (proc.waitForFinished()==true)
        {
           b= proc.readAllStandardOutput();
           encryptString=QString(b);
           return true;
          }
        else
        {
         b=proc.readAllStandardError();
         cout<<"Error:Not encrypt string!"<<endl;
         return false;
        }

}

QString Yandex::ParsStr(QString data,QString begin,QString end)
{
    QRegExp rx(begin+"([^<]*)"+end);
            rx.indexIn(data);
            return rx.cap(1);

}

QStringList Yandex::ParsList(QString data,QString begin,QString end)
{
     QStringList list;
    QRegExp rx(begin+"([^<]*)"+end);
            rx.setMinimal(true);

             int pos = 0;
             while ((pos = rx.indexIn(data, pos)) != -1)
             {
                list << rx.cap(1);
                 pos += rx.matchedLength();
             }
             return list;
}

void Yandex::ParsKeyId(QString str)
{
    key= ParsStr(str,"<key>","</key>");
    id = ParsStr(str,"<request_id>","</request_id>");

}

void Yandex::ParsTocken(QString str)
{
    token=ParsStr(str,"<token>","</token>");
}
