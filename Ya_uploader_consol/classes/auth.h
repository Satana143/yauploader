#ifndef AUTH_H
#define AUTH_H

#include "yandex.h"

#include <QObject>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QtNetwork/QNetworkRequest>
#include <QUrl>
#include <QString>
#include <QByteArray>

class Auth : public QObject
{
    Q_OBJECT        
public:
    explicit Auth(QObject *parent = 0);
    Auth(QString login, QString pass);

    void KeyId();
    QByteArray getTocken();
private:
    QNetworkAccessManager manager;
    QNetworkReply *reply;
    QUrl apiUrl;
    QString answer;
    QByteArray Authorization; //автаризационный токен
    Yandex ya;//парс и шифрование
    int stage;//стадия авторизации
    int authCount;
    QString log;
    QString pass;
signals:
    void finish();
    void errorAuth();
public slots:
    void readyReadReply();
    void getReplyFinished();
    void login();
    void goLogin();
};

#endif // AUTH_H
