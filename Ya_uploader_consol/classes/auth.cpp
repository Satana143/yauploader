﻿#include "auth.h"
#include <iostream>

using namespace std;

Auth::Auth(QObject *parent) :
    QObject(parent)
{
}
Auth::Auth(QString login,QString pass){
    this->log=login;
    this->pass=pass;
}

//запуск авторизации
void  Auth::goLogin()
{
    //чистим переменные
    ya.encryptString.clear();;
    ya.id.clear();;
    ya.key.clear();
    ya.token.clear();
    answer.clear();
    /*Устанавливаем стадию работы программы о 0.
    (0-работа в офф.лайн;1-получение ключа и id; 2 получение токена;3 - работа с сервисом )*/
    stage=0;
    authCount=0;
    //запускаем
    KeyId();
}

//получение ключа и ид
void Auth::KeyId()
{
  // cout<<"Get Key&id\n";
  stage=1;
  apiUrl="http://auth.mobile.yandex.ru/yamrsa/key/";

  QNetworkRequest request(apiUrl);
  reply = manager.get(request);


  connect(reply, SIGNAL(finished()),this, SLOT(getReplyFinished()));
  connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadReply()));
}

QByteArray Auth::getTocken()
{
    return this->Authorization;
}
//ответ от сервера
void Auth::readyReadReply()
{
  switch (stage)
  {
      case 1:
          {
              answer = QString::fromUtf8(reply->readAll());
              ya.ParsKeyId(answer);

              // запускаем функцию логина.
               connect(reply, SIGNAL(finished()),this, SLOT(login()));
             break;
          }
      case 2:
          {
              answer = QString::fromUtf8(reply->readAll());
              ya.ParsTocken(answer);

              if(!ya.token.isEmpty())
              {
               Authorization.append("FimpToken realm=\"fotki.yandex.ru\", token=\"" + ya.token + "\"");
               emit finish();
              }else
              {
                  if(authCount!=50)
                  {
                    authCount++;
                    connect(reply, SIGNAL(finished()),this, SLOT(goLogin()));
                  }else
                  cout<<"Authorization error. Check the username and password or try again later."<<endl;
              }
              break;
          }
  }
}

void Auth::getReplyFinished()
{
reply->deleteLater();
}

void Auth::login()
{
  stage=2;
  apiUrl="http://auth.mobile.yandex.ru/yamrsa/token/";
  QString credentials("<credentials login=\""+log+"\" password=\""+pass+"\"/>");

  if (ya.Encrypt(ya.key,credentials))
  {

  QString str =  "request_id=" + ya.id + "&credentials=" + ya.encryptString;

  QByteArray requestString;
  requestString.clear();
  requestString.append(str);

  QNetworkRequest request(apiUrl);
  reply = manager.post(request, requestString);


  connect(reply, SIGNAL(finished()),this, SLOT(getReplyFinished()));
  connect(reply, SIGNAL(readyRead()), this, SLOT(readyReadReply()));
  }else{
      emit errorAuth();
  }
}
